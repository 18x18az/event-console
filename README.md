[![Event Console Overview Video](https://www.18x18az.org/img/event-console-video.png)](https://youtu.be/-hYVw1OZM5o)

<br>

**Event Console server:** https://gitlab.com/18x18az/event-console/-/tree/server
- Python 3.8
- Set up `venv` and run `pip install -r requirements.txt`
- Run by calling `main.py`

<br>

**Event Console frontend:** https://gitlab.com/18x18az/event-console/-/tree/frontend
- Designed for deployment with GitLab Pages
- `stream` folder does not need to be widely deployed
- Yes, it is pretty messy
- Yes, it should be using React

<br>

**Issues, merge requests, and feature requests:** Message Barin on VEX&nbsp;Forum or Discord

<br>

All code released under the Apache 2.0 license.
